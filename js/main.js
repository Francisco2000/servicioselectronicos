// JavaScript Document

$(function(){
	$("[data-toggle='popover']").popover();
	$("[data-toggle='tooltip']").tooltip();
	
	$('#exampleModal').on('click',function(){
		console.log("el modal se esta ejecutando");
		
		$('#btnContacto').removeClass("btn-dark");
		$('#btnContacto').addClass("btn-success");
	});
	
	$('#exampleModal').on('show.bs.modal',function(){
		console.log("el modal se mostro");
	});
	
	$('#exampleModal').on('hide.bs.modal',function(){
		console.log("el modal se oculto");
	});
	
	$('#exampleModal').on('hidden.bs.modal',function(){
		console.log("el modal se cerro");
		$('#btnContacto').removeClass("btn-success");
		$('#btnContacto').addClass("btn-dark");
	});
	
	
});