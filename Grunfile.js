module.exports = function(grunt){
	grunt.initConfig({
		sass: {
			dist: {
				files: [{
					expand: true,y
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css',
					ext: '.css'
				}]
			}
		},
		watch:{
			files: ['css/*.scss'],
			tasks: ['css']
		},
		
		browserSync: {
			dev: {
				bsFiles: {
					src: [
						'css/*.css',
						'*.html',
						'js/*.js'
					]
				},
				options{
				watchTask: true,
				server: {
				baseDir: './'
			  }
			 }
			},
			options: {
			watchTask: true,
			server: {
				baseDir: './'		 
			}
		 }
		},
	    imagemin:{
			dynamic:{
				files:[{
					expand: true,
					 cwd: './',
					 src: 'img/*.{png,gif,jpg,jpeg}'
				}]		 
			}			 
		},
		

	});
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.registerTask('css',['sass']);
	grunt.registerTask('default',['browserSync','Watch']);
	grunt.registerTask('img:compress',['imagemin']);
};